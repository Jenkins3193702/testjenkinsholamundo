#Establecemos la imagen base
FROM tomcat:8.0-alpine

# etiquetas
LABEL Desc="Prueba test jenkins" version="1.0"

#Agregamos war al folder webapps
ADD pruebaJenkins-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/

#Que salga en el puerto 8080
EXPOSE 8080

#Iniciamos el servidor tomcat
CMD ["catalina.sh", "run"]
